module.exports = function() {

  $.gulp.task('watch', ()=> {
    $.gulp.watch(pug.watch, $.gulp.series('pug'));
    $.gulp.watch(json.watch, $.gulp.series('json'));
    $.gulp.watch(styles.watch, $.gulp.series('styles:dev'));
    $.gulp.watch(js.watch, $.gulp.series('js:dev'));
    $.gulp.watch(img.watch, $.gulp.series('img:dev'));
    $.gulp.watch(fonts.watch, $.gulp.series('fonts'));
    $.gulp.watch(iconfont.watch, $.gulp.series('iconfont'));
  });

};