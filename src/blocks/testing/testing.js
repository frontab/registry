var isTimer = $('.testing-time');

if ( isTimer ) {
  testingTimer();
}

function testingTimer() {
  var box = $('.testing-time__val'),
      time = 1,
      m = '00',
      s = '00',
      timeResult = m + ':' + s;

  box.html(timeResult);

  var testingTimer = setInterval(function() {

        s = time % 60;
        m = parseInt(time / 60);

        if (s < 10) {
          s = "0" + s;
        }

        if (m < 10) {
          m = "0" + m;
        }

        timeResult = m + ':' + s;

        box.html(timeResult);

        time++;
      }, 1000);

}

$('.testing-controls .btn').on('click', function(e) {
  e.preventDefault();

  var questionActive = $('.testing-step.active');

  if ( questionActive.next().length < 1 ) {
    $.fancybox.open({
      src: '#popup-testing-bad'
    });
  } else {
    questionNext(questionActive);
  }
});

function questionNext(questionActive) {
  questionActive.next()
                .addClass('active')
                .siblings()
                .removeClass('active');

  $('.testing-item.active').next()
                           .addClass('active')
                           .siblings()
                           .removeClass('active');

  var step = $('.testing-iteration-step').html();
  step = parseInt(step) + 1;
  $('.testing-iteration-step').html(step);
}