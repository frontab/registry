$('.js-box-edit').on('click', function(e) {
  e.preventDefault();

  $(this).closest('.prof-box')
         .toggleClass('edit');

  // $(this).attr('disabled',"disabled");
});

$('.prof-box .prof-box__action .btn').on('click', function(e) {
  e.preventDefault();

  $(this).closest('.prof-box')
         .removeClass('edit');
});