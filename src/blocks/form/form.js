$('select').styler({
  onFormStyled: function() {
    $('.jq-selectbox__dropdown ul').scrollbar();
  }
});

$('.prof-box__res').scrollbar();


$('input[type=tel]').mask("+7 (999) 999-99-99");

var rangeAge = document.getElementById('range-age');

if ($('#range-age').length) {
  var rangeAgeSlider = noUiSlider.create(rangeAge, {
      range: {
          'min': 18,
          'max': 90
      },
      start: [18, 90],
      step: 1,
      connect: true,
      behaviour: 'tap-drag',
      tooltips: true,
      pips: {
        mode: 'steps',
        stepped: true,
        density: 4,
        format: wNumb({
            decimals: 0
        })
      },
      format: wNumb({
          decimals: 0,
          prefix: ' руб'
      })
  });

  var nodes = [
    document.getElementById('range-age-lower'),
    document.getElementById('range-age-upper')
  ];

  rangeAgeSlider.on("slide", function (values, handle, unencoded, isTap, positions) {
    nodes[handle].value = values[handle];
  });
}

$('.form-comb-type').on('change', function() {
  var val = $(this).val(),
      box = $(this).closest('.prof-box'),
      combs = box.find('.form-comb');

  combs.each(function() {
    if ( $(this).attr('data-box') == val) {
      $(this).addClass('active')
             .siblings('.form-comb')
             .removeClass('active');
    }
  });
});