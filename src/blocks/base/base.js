$('select.sel-main-1').on('change', function() {
  if ( $(this).val() ) {
    $('select.sel-main-2').prop('disabled',false);
    $('select.sel-main-2').closest('.jq-selectbox')
                          .removeClass('disabled');
    $('select.sel-main-2').styler('destroy');
    $('select.sel-main-2').styler({
      onFormStyled: function() {
        $('.jq-selectbox__dropdown ul').scrollbar();
      }
    });
  }
});
$('select.sel-main-2').on('change', function() {
  if ( $(this).val() ) {
    $('select.sel-main-3').prop('disabled',false);
    $('select.sel-main-3').closest('.jq-selectbox')
                          .removeClass('disabled');
    $('select.sel-main-3').styler('destroy');
    $('select.sel-main-3').styler({
      onFormStyled: function() {
        $('.jq-selectbox__dropdown ul').scrollbar();
      }
    });
  }
});
