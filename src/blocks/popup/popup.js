$('.js-popup').on('click', function(e) {
  e.preventDefault();
  if ( $('body').hasClass('fancybox-active') ) {
    $('[data-fancybox-close]').trigger('click');
  }
  $.fancybox.open({
    src: $(this).attr('href'),
    hash : false
  });
});


$('.popup-reg__tabs .btn').on('click', function(e) {
  e.preventDefault();
  var attr = $(this).attr('data-box');

  $(this).addClass('active')
         .siblings()
         .removeClass('active');

  $(this).closest('.popup-reg')
         .addClass('popup-reg--full');

  $('.popup-reg__box').each(function() {
    if ( $(this).attr('data-box') == attr ) {
      $(this).addClass('active')
             .siblings()
             .removeClass('active');
    }
  });
});