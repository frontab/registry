$('.accordion-name').on('click', function(e) {
  e.preventDefault();
  $(this).closest('.accordion')
         .toggleClass('active');

  $(this).closest('.accordion')
         .find('.accordion-content')
         .stop()
         .slideToggle(500);
});

$('.accordion').each(function() {
  if ( $(this).hasClass('active') ) {
    $(this).find('.accordion-content').css({'display':'block'});
  }
});
