$(window).on('scroll', function() {
  navAsideScroll();
});

navAsideScroll();

function navAsideScroll() {
  if ( $('.nav-aside').length > 0 ) {
    var headerH = $('.header').height();
    if ( $(window).scrollTop() > headerH ) {
      $('.nav-aside').addClass('scrolled');
    } else {
      $('.nav-aside').removeClass('scrolled');
    }
  }
}